<%@page import="java.io.File,cons.MyFilter"%>
<script  type="text/javascript" src="scripts/jquery-1.9.0.min.js"></script>

<link rel=stylesheet href="scripts/editor/doc/docs.css"/>
<link rel="stylesheet" href="scripts/editor/theme/3024-night.css"/>
<link rel="stylesheet" href="scripts/editor/lib/codemirror.css"/>
<script src="scripts/editor/lib/codemirror.js"></script>
<script language="text/javascript">

    var back=0;
    var prev=1;
    var title;
    $(document).ready(function() {
    $("#cancel3,#cross3").click(function() {
        $("#bg").hide();
        $("#save_dialog").hide();
    })
    $("#back_next_btn").click(function() {
        
        if(back>0)
            {
                $.ajax({  
                type: "POST",
                url: "browsedirectory.jsp?path=<%=session.getAttribute("userid").toString()%>",
                data: "",
                success: function(msg) {
                    $("#save_dialog_dynamic").html(msg);        
                },
                error: function(ob,errStr) {
                            alert("hello error");
                            }
                })
                prev=prev+1;
                back=back-1;
            }
    })
    $("#forward_next_btn").click(function() {
        if(prev>0)
            {
                var val="<%=session.getAttribute("userid").toString()%>"+"/"+title;
                $.ajax({  
                    type: "POST",
                    url: "browsedirectory.jsp?path="+val,
                    data: "",
                    success: function(msg) {
                        $("#save_dialog_dynamic").html(msg);  
                        $("#file_name").val(v);
                        var fp=$("#file_path_textbox").val()+"/"+$("#file_name").val();
                        $("#file_path_textbox").val(fp);
                    },
                    error: function(ob,errStr) {
                        $("#file_name").val(title);
                    }
                })
                prev=prev-1;
                back=back+1;
            }
    })
    $("#cancel9,#cross9").click(function() {
        $("#bg").hide();
        $("#file_dialog").hide();
    })
    $("#file_dialog_yes").click(function(){
        var jsonObj={"content": editor.getValue()};
        $.ajax({  
            type: "POST",
            url: "saveas",
            data: jsonObj,
            success: function(msg) {
                $("#bg").hide();
                $("#file_dialog").hide();
                $("#save_dialog").hide();
                $("#title_IDE").html(msg+"-Online Compiler IDE");
            },
            error: function(ob,errStr) {
                
            }
        })
    })
    $("#ok").click(function(){
        var filepath=$("#file_path_textbox").val();
        var jsonObj={"filename": filepath+"/"+$("#file_name").val(),"content": editor.getValue()};
        $.ajax({  
            type: "POST",
            url: "saveFile",
            data: jsonObj,
            success: function(msg) {
                if(msg==1)
                {
                    $("#bg").show();
                    $("#file_dialog").show();
                }
                else
                {
                    $("#bg").hide();
                    $("#save_dialog").hide();
                    $("#title_IDE").html(msg+"-Online Compiler IDE");
                }
            },
            error: function(ob,errStr) {
              
            }
        })
    })
    $(".openFile_ul li span").click(function(e) {
         e.preventDefault();
         var v=$(this).attr("title");
         var val="<%=session.getAttribute("userid").toString()%>"+"/"+$(this).attr("title");
         $.ajax({  
            type: "POST",
            url: "browsedirectory.jsp?path="+val,
            data: "",
            success: function(msg) {
                $("#save_dialog_dynamic").html(msg);  
                $("#file_name").val(v);
                var fp=$("#file_path_textbox").val()+"/"+$("#file_name").val();
                $("#file_path_textbox").val(fp);
                back=back+1;
                title=v;
            },
            error: function(ob,errStr) {
                $("#file_name").val(v);
            }
        })
      })
    })
</script>
<style>
    #dirselect{
        text-decoration: none;
        cursor: default;
    }
    #dirselect:hover{
        text-decoration: underline;
        cursor: pointer;
    }
</style>   
<div class="head" style=" border-bottom:1px solid #5e7e9d;"><span class="logo_icon"><img src="images/save_file.png" /></span>
    <span class="notepad">Save</span>
    <span class="cross" id="cross3"><img src="images/cross.png" /></span>
</div>
<%
    String name=session.getAttribute("name").toString();
    name=name.substring(0, name.indexOf(" "));
%>
<div class="Dialog_Head"><span class="pre_next" id="back_next_btn"><img src="images/pre.png"/></span><span class="pre_next" id="forward_next_btn"><img src="images/next.png" /></span><span class="down_arrow"><img src="images/down_arrow.png" /></span><div class="file_path"><input type="text" class="file_path_textbox" id="file_path_textbox" value="ftp://server/<%=name%>"<%
    String path=request.getParameter("path");
    File f=new File(application.getRealPath("/")+path);
    File files[]=f.listFiles(new MyFilter());
%>></div></div>
<ul class="openFile_ul">
<li></li>
<%
    for(int i=0;i<files.length;i++)
    {
%>
<li><span id="dirselect" title="<%=files[i].getName()%>"><%=files[i].getName()%></span></li>
<%
    }
%>
</ul>
<div class="file_wrap">
<div class="dialog_btn" style="margin:10px 0;">
<input type="text" class="open_list" id="file_name"/>
<span style="float:right;">File Name</span>
</div>
<div class="dialog_btn" style="margin-bottom:10px;">
<select class="savetype_list">
<option>Document Type (all type)</option>
</select>
<span style="float:right;">Save as Type</span>
</div>
<div  class="dialog_btn"><input type="submit" class="Fbutton" value="Cancel" id="cancel3"/><input type="submit" class="Fbutton" value="Ok" id="ok"/>
</div>
</div>
<div class="dialog" id="file_dialog" style="width:300px; margin-top:0px; " >
<div class="dialog_upper" style="width:300px;" >
<div class="dialog_box" style="margin-top:100px; background:#efefef;">
<div class="head" style=" border-bottom:1px solid #5e7e9d;"><span class="logo_icon"><img src="images/logo.png" /></span><span class="notepad">Online Compiler IDE</span>
<span class="cross" id="cross9"><img src="images/cross.png" /></span>
</div>
<div class="dialog_btn" style="margin:10px 0;">
<span style="float:left; margin:3px 22px;">Do you really want to replace this file</span>
</div>
<div class="dialog_btn"> 
<span style="float:right; margin-top:12px;"><input type="submit" class="Fbutton" value="No" id="cancel9" style="height:30px; width:80px;" /><input type="submit" class="Fbutton" value="Yes" id="file_dialog_yes" style="height:30px; width:80px;" /></span>
</div>
</div>
</div>
</div>