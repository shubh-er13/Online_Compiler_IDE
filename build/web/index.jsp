<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login Page</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="main_wrapper">
<div class="main_head">
    <div class="logo"><img src="images/img.png" width="120" height="100"/></div>
</div>
<div class="background_bg" id="bg"></div>
<div class="out_wrapper" style="width:500px; ">
<div class="wrapper" style="height:auto; background:#ACC3D9	;">
<div class="head">
<span class="logo_icon"><img src="images/logo.png" /></span><span class="notepad">Online Compiler IDE Login</span>
</div><!--head-->
<div class="menu">
<div class="menu_ul"></div>
<ul class="login_ul">   
<form name="frm" action="login" method="post">     
    <li><span>UserID</span><input type="text" class="textbox" name="userid" /></li>
<li><span>Password</span><input type="password" class="textbox"  name="passwd"/></li>
<li><input type="submit" class="Lbutton" style="margin-bottom:0;" /></li>
<span style="color:red;font-family: verdana;font-size: 13px;"><%
    String msg=request.getParameter("msgerr");
    String err=request.getParameter("err");
    if(msg!=null)
        out.println("Either userid or password is incorrect");
    else if(err!=null)
        out.println("your email is not verified yet");
   %></span>
</form>   
<li><span class="reg_heading"></span><img src="images/hand.png" /></span><input type="submit" value="Sign Up Here" class="reg_btn" style="cursor: pointer" onclick="javascript:window.location='register.jsp'"/></span></li>
</ul>
</div>
</div>
</div>
</div>
</body>
</html>
