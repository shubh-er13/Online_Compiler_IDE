<%@page import="java.sql.*,cons.Connections,java.io.*"%>
<%@page errorPage="index.jsp"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Welcome Page</title>
        <link href="style.css" rel="stylesheet" type="text/css" />
        <script src="scripts/jquery-1.9.0.min.js"></script>
        
        <link rel=stylesheet href="scripts/editor/doc/docs.css"/>
        <link rel="stylesheet" href="scripts/editor/theme/3024-night.css"/>
        <link rel="stylesheet" href="scripts/editor/lib/codemirror.css"/>
        <script src="scripts/editor/lib/codemirror.js"></script>
        <script src="scripts/editor/addon/edit/matchbrackets.js"></script>
        <script src="scripts/editor/addon/edit/closebrackets.js"></script>
        
        <link rel="stylesheet" href="scripts/editor/addon/hint/show-hint.css">
        <script src="scripts/editor/addon/hint/show-hint.js"></script>
        
        <script src="scripts/editor/mode/clike/clike.js"></script>
        <script src="scripts/editor/addon/selection/active-line.js"></script>
        
        <link rel="stylesheet" href="scripts/editor/theme/3024-day.css">
        <link rel="stylesheet" href="scripts/editor/theme/3024-night.css">
        <link rel="stylesheet" href="scripts/editor/theme/vibrant-ink.css">
        
        
        <script>
            var str=""; 
            var e=0;
            var f=0;
            var sum=0;
            var val11="";
            var editor="";
            $(document).ready(function() {
                $("#menu1").click(function() {
                    $("#sub1").toggle();
                    $("#sub2").hide();
                    $("#sub3").hide();
                    $("#sub4").hide();
                    $("#sub5").hide();
                });
                $("#sub1").mouseleave(function() {
                    $("#sub1").hide();
                });
                $("#menu2").click(function() {
                    $("#sub2").toggle();
                    $("#sub1").hide();
                    $("#sub3").hide();
                    $("#sub4").hide();
                    $("#sub5").hide();
                });
                $("#sub2").mouseleave(function() {
                    $("#sub2").hide();
                });
                $("#menu3").click(function() {
                    $("#sub3").toggle();
                    $("#sub2").hide();
                    $("#sub1").hide();
                    $("#sub4").hide();
                    $("#sub5").hide();
                });
                $("#sub3").mouseleave(function() {
                    $("#sub3").hide();
                });
                $("#menu4").click(function() {
                    $("#sub4").toggle();
                    $("#sub2").hide();
                    $("#sub3").hide();
                    $("#sub1").hide();
                    $("#sub5").hide();
                });
                $("#sub4").mouseleave(function() {
                    $("#sub4").hide();
                });
                $("#menu5").click(function() {
                    $("#sub5").toggle();
                    $("#sub2").hide();
                    $("#sub3").hide();
                    $("#sub4").hide();
                    $("#sub1").hide();
                });
                $("#sub5").mouseleave(function() {
                    $("#sub5").hide();
                });
                $("#open").click(function() {
                    $("#bg").show();
                    $("#open_dialog").show();
                    $("#bg").show();
                    $("#open_dialog").show();
                    $.ajax({  
                        type: "POST",
                        url: "opendirectory.jsp?path=<%=session.getAttribute("userid").toString()%>",
                        data: "",
                        success: function(msg) {
                            $("#open_dialog_dynamic").html(msg);        
                        },
                        error: function(ob,errStr) {
                            alert("hello error");
                        }
                    })
                });
                $("#tool_save").click(function() {
                    $("#save").click();
                });
                $("#tool_new").click(function() {
                    $("#new").click();
                });
                $("#tool_open").click(function() {
                    $("#open").click();
                });
                $("#tool_copy").click(function() {
                    $("#copy").click();
                });
                $("#tool_cut").click(function() {
                    $("#cut").click();
                });
                $("#tool_paste").click(function() {
                    $("#paste").click();
                });
                $("#tool_bold").click(function() {
                    $("#write_area").css("font-weight", "bold");
                });
                $("#tool_italic").click(function() {
                    $("#write_area").css("font-style", "italic");
                });
                $("#tool_underline").click(function() {
                    $("#write_area").css("text-decoration", "underline");
                });
                $("#cancel11,#cross11").click(function() {
                    $("#bg").hide();
                    $("#question_file_dialog").hide();
                })
                $("#file_dialog_no").click(function() {
                    $("#bg").hide();
                    $("#question_file_dialog").hide();
                    editor.setValue("");
                    $("#sub1").hide();
                    $("#title_IDE").html("Untitled-Online Compiler IDE");
                });
                $("#file_dialog_yes").click(function() {
                    $("#bg").hide();
                    $("#question_file_dialog").hide();
                    $("#save").click();
                });
                $("#new").click(function() {
                    var filename=$("#title_IDE").html();
                    var content=editor.getValue();
                    if(filename.indexOf("Untitled")!=-1&&content!='')
                    {
                        $("#bg").show();
                        $("#question_file_dialog").show();
                    }
                    else
   
                    {
                        editor.setValue("");
                        $("#sub1").hide();
                        $("#title_IDE").html("Untitled-Online Compiler IDE");
                    }
                });
                $("#cancel2,#cross2").click(function() {
                    $("#bg").hide();
                    $("#open_dialog").hide();
                });
                $("#save").click(function() {
                    var filename=$("#title_IDE").html();
                    if(filename.indexOf("Untitled")!=-1)
                    {
                        $("#bg").show();
                        $("#save_dialog").show();
                        $.ajax({  
                            type: "POST",
                            url: "browsedirectory.jsp?path=<%=session.getAttribute("userid").toString()%>",
                            data: "",
                            success: function(msg) {
                                $("#save_dialog_dynamic").html(msg);        
                            },
                            error: function(ob,errStr) {
                                alert("hello error");
                            }
                        })
                    }
                    else
                    {
                        var jsonObj={"content": editor.getValue()};
                        $.ajax({  
                            type: "POST",
                            url: "saveas",
                            data: jsonObj,
                            success: function(msg) {
                                $("#title_IDE").html(msg+"-Online Compiler IDE");
                                $("#sub1").hide();
                            },
                            error: function(ob,errStr) {
                
                            }
                        })
                    }
                });
                $("#cancel3,#cross3").click(function() {
                    $("#bg").hide();
                    $("#save_dialog").hide();
                });
                $("#save_as").click(function() {
                    $("#bg").show();
                    $("#save_dialog").show();
                    $.ajax({  
                        type: "POST",
                        url: "browsedirectory.jsp?path=<%=session.getAttribute("userid").toString()%>",
                        data: "",
                        success: function(msg) {
                            $("#save_dialog_dynamic").html(msg);        
                        },
                        error: function(ob,errStr) {
                            alert("hello error");
                        }
                    })
                });
                $("#cancel4,#cross4").click(function() {
                    $("#bg").hide();
                    $("#saveas_dialog").hide();
                });
                $("#font").click(function() {
                    $("#bg").show();
                    $("#font_dialog").show();
                });
                $("#cancel1,#cross1").click(function() {
                    $("#bg").hide();
                    $("#font_dialog").hide();
                });
                $("#about").click(function() {
                    $("#bg").show();
                    $("#about_dialog").show();
                });
                $("#cancel5,#cross5").click(function() {
                    $("#bg").hide();
                    $("#about_dialog").hide();
                });
                $("#find").click(function () {
                    $("#bg").show();
                    $("#find_dialog").show();
                });
                $("#cancel6,#cross6").click(function () {
                    $("#bg").hide();
                    $("#find_dialog").hide();
                });
                $("#goto").click(function () {
                    $("#bg").show();
                    $("#goto_dialog").show();
                });
                $("#cancel7,#cross7").click(function () {
                    $("#bg").hide();
                    $("#goto_dialog").hide();
                });
                $("#replace").click(function () {
                    $("#bg").show();
                    $("#replace_dialog").show();
                });
                $("#cancel8,#cross8").click(function () {
                    $("#bg").hide();
                    $("#replace_dialog").hide();
                });
                $("#font_name li span").click(function() {
                    $("#text_name").val($(this).html());
                    document.getElementById("sample_text").setAttribute("style", "font-family: "+$(this).html());
                });
                $("#font_style li span").click(function() {
                    $("#text_style").val($(this).html());
                    document.getElementById("sample_text").setAttribute("style", "font-weight: "+$(this).html()+"; font-family: "+$("#text_name").val());
                    //document.getElementById("sample_text").style.fontSize=$("#text_size").val()+"px";
                });
                $("#font_size li span").click(function() {
                    $("#text_size").val($(this).html());
                    document.getElementById("sample_text").setAttribute("style","font-family: "+$("#text_name").val()+"; font-weight: "+$("#text_style").val());
                    document.getElementById("sample_text").style.fontSize=$("#text_size").val()+"px";
                });
               $("#font_ok").click(function() {
                    $("#bg").hide();
                    $("#font_dialog").hide();
                    document.getElementById("write_area").setAttribute("style", "font-family: "+$("#text_name").val()+"; font-weight: "+$("#text_style").val());
                    document.getElementById("write_area").style.fontSize=$("#text_size").val()+"px";
                });
                $("#cut").click(function() {
                    $("#sub2").hide();
                    e=1;
                    var textField=document.getElementById('write_area');
                    var string=editor.getValue();
    
                    var start=textField.selectionStart;
                    var end=textField.selectionEnd;
                    if(end-start>0){
                        str=string.substring(start, end);
                        var st1=string.substring(0,start);
                        var st2=string.substring(end+1, string.length);
                        var st3=st1.concat(st2);
                        $("#write_area").val(st3);
                    }
                });
                $("#copy").click(function() {
                    $("#sub2").hide();
                    var textField=document.getElementById('write_area');
                    var start=textField.selectionStart;
                    var end=textField.selectionEnd;
                    str=textField.value.substring(start, end);
                });
                $("#delete").click(function() {
                    $("#sub2").hide();
                    var textField=document.getElementById('write_area');
                    var string=document.getElementById('write_area').value;
                    var start=textField.selectionStart;
                    var end=textField.selectionEnd;
                    if(end-start>0){
                        var st1=string.substring(0,start);
                        var st2=string.substring(end+1, string.length);
                        var st3=st1.concat(st2);
                        $("#write_area").val(st3);
                    }
                });
                $("#paste").click(function() {
                    $("#sub2").hide();
                    if(str!="")
                    {
                        var textField=document.getElementById('write_area');
                        var string=document.getElementById('write_area').value;
                        var start=textField.selectionStart;
                        var st1=textField.value.substring(0,start);
                        var st2=textField.value.substring(start,string.length);
                        var st3=st1+str+st2;//.concat(str);
                        //var st4=st3.concat(st2);
                        document.getElementById('write_area').value=st3;
                        if(e==1)
                        {
                            str="";
                            e=0;
                        }
                    }
                });
                $("#timedate").click(function(){
                    //alert("hello");
                    date = new Date();
                    year = date.getFullYear();
                    month = date.getMonth();
                    months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
                    d = date.getDate();
                    day = date.getDay();
                    days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
                    h = date.getHours();
                    if(h<10)
                    {
                        h = "0"+h;
                    }
                    m = date.getMinutes();
                    if(m<10)
                    {
                        m = "0"+m;
                    }
                    s = date.getSeconds();
                    if(s<10)
                    {
                        s = "0"+s;
                    }
                    result = ''+days[day]+' '+months[month]+' '+d+' '+year+' '+h+':'+m+':'+s;
                    var index=document.getElementById('write_area').selectionEnd;
                    var content=$("#write_area").val();
                    $("#write_area").val(content.substring(0, index)+""+result+""+content.substring(index));
                    $("#sub2").hide();
                })
                $("#select").click(function(){
                    var textArea=document.getElementById('write_area');
                    textArea.selectionStart=0;
                    textArea.selectionEnd=$("#write_area").val().length;
                    $("#sub2").hide();
                });
                $("#replace_text").click(function(){
                    var textArea=document.getElementById('write_area');
                    var textField_val=document.getElementById('replace_find_text').value;
                    var textArea_val=textArea.value;
                    var textField_replace_val=document.getElementById('replace_replace_text').value;
                    if(!(document.getElementById("replace_match_case").checked))
                    {
                        textArea_val=textArea_val.toLowerCase();
                        textField_val=textField_val.toLowerCase();
                        textField_replace_val=textField_replace_val.toLowerCase();
                    }
                    var val8=textArea_val.replace(textField_val,textField_replace_val);
                    $("#write_area").val(val8);
                });
                $("#replace_all").click(function(){
                    var textArea=document.getElementById('write_area');
                    var textField_val=document.getElementById('replace_find_text').value;
                    var textArea_val=textArea.value;
                    var textField_replace_val=document.getElementById('replace_replace_text').value;
                    if(!(document.getElementById("replace_match_case").checked))
                    {
                        textArea_val=textArea_val.toLowerCase();
                        textField_val=textField_val.toLowerCase();
                        textField_replace_val=textField_replace_val.toLowerCase();
                    }
                    var val8=textArea_val.replace(new RegExp(textField_val, 'g'),textField_replace_val);
                    $("#write_area").val(val8);
                });
                $("#replace_find_next").click(function(){
                    var textArea=document.getElementById('write_area');
                    var textField_val=document.getElementById('replace_find_text').value;
                    var textArea_val=textArea.value;
                    if(!(document.getElementById("replace_match_case").checked))
                    {
                        textArea_val=textArea_val.toLowerCase();
                        textField_val=textField_val.toLowerCase();
                    }
                    var pos="";   
                    pos=textArea.selectionEnd;
                    var srchText=textArea_val.substring(pos);
                    var index=0
                    if((index=srchText.indexOf(textField_val))!=-1)
                    {
                        textArea.selectionStart=pos+index;
                        textArea.selectionEnd=pos+index+textField_val.length;
                    }
                    else
                    {
                        alert("Cannot find search string \""+textField_val+"\"");
                    }
                });
                $("#find_next").click(function(){
                    var radioButtons = document.getElementsByName("dir");
                    var textArea=document.getElementById('write_area');
                    var textField_val=document.getElementById('find_text').value;
                    var textArea_val=textArea.value;
                    if(!(document.getElementById("match_case").checked))
                    {
                        textArea_val=textArea_val.toLowerCase();
                        textField_val=textField_val.toLowerCase();
                    }
                    var pos="";
                    if (radioButtons[0].checked) 
                    {   
                        pos=textArea.selectionStart;
                        var srchText=textArea_val.substring(0,pos);
                        var index=0
                        if((index=srchText.lastIndexOf(textField_val))!=-1)
                        {
                            textArea.selectionStart=index;
                            textArea.selectionEnd=index+textField_val.length;
                        }
                        else
                        {
                            alert("Cannot find search string \""+textField_val+"\"");
                        }    
                    }
                    else if(radioButtons[1].checked)
                    {
                        pos=textArea.selectionEnd;
                        var srchText=textArea_val.substring(pos);
                        var index=0
                        if((index=srchText.indexOf(textField_val))!=-1)
                        {
                            textArea.selectionStart=pos+index;
                            textArea.selectionEnd=pos+index+textField_val.length;
                        }
                        else
                        {
                            alert("Cannot find search string \""+textField_val+"\"");
                        }
                    } 
                });
                $("#er_cross").click(function () {
                    $("#textarea").removeClass("er_textarea");
                    $("#er_window").hide();
                })
                $("#compile").click(function () {
                    $("#sub4").hide();
                    var filename=$("#title_IDE").html();
                    filename=filename.substring(0, filename.indexOf("-"));
                    var jsonObj={"filename": filename};
                    $.ajax({  
                        type: "POST",
                        url: "compile",
                        data: jsonObj,
                        success: function(msg) {
                            msg=msg.trim();
                            if(msg=="")
                                msg='No Syntax Error';
                            $("#textarea").addClass("er_textarea");
                            $("#er_window").show();
                            $("#output_area").val(msg);
                        },
                        error: function(ob,errStr) {
                
                        }
                    })
                })
                $("#run").click(function () {
                    $("#sub4").hide();
                    var filename=$("#title_IDE").html();
                    filename=filename.substring(0, filename.indexOf("-"));
                    var jsonObj={"filename": filename};
                    $.ajax({  
                        type: "POST",
                        url: "run",
                        data: jsonObj,
                        success: function(msg) {
                            
                            $("#textarea").addClass("er_textarea");
                            $("#er_window").show();
                            $("#output_area").val(msg);
                        },
                        error: function(ob,errStr) {
                
                        }
                    })
                })
            });
            
          
        
               
        </script>
    </head>
    <body>
        <div class="main_wrapper">
            <div class="main_head">
                <div class="logo"><img src="images/img.png" width="120" height="100"></div>
                <ul class="head_menu">
                    <li><a href="index.jsp">Logout</a></li>
                    <li><a href="welcome.jsp">Help</a></li>
                    <li><a href="knowledge_base.jsp">Knowledge Base</a></li>
                    <%
                        String name = session.getAttribute("name").toString();
                        String gender = session.getAttribute("gen").toString();
                    %>
                    <li><a href="welcome.jsp"><%=name%></a></li>
                    <%
                        if (gender.equals("male")) {
                    %>
                    <li><div class="user_img"><img src="images/male_user.png" /></div></li>        
                    <%        } else {
                    %>
                    <li><div class="user_img"><img src="images/user.png" /></div></li>        
                    <%            }
                    %>
                </ul>
            </div>
            <div class="background_bg" id="bg"></div>
            <div class="out_wrapper">
                <div class="wrapper">
                    <div class="head">
                        <span class="logo_icon"><img src="images/logo.png" /></span><span class="notepad" id="title_IDE">Untitled-Online Compiler IDE </span>
                        <span class="cross"><img src="images/cross.png" /></span><span class="cross"><img src="images/minimize.png" /></span><span class="cross"><img src="images/maximize.png" /></span>
                    </div><!--head-->
                    <div class="menu">
                        <ul class="menu_ul">
                            <li class="menu_li"><span id="menu1">File</span>
                                <ul class="SUBmenu_ul" id="sub1">
                                    <li id="new"><span class="Lsub">New</span> <span class="Rsub">Ctrl+N</span></li>
                                    <li id="open"><span class="Lsub">Open</span> <span class="Rsub">Ctrl+O</span></li>
                                    <li id="save" lang="abc"><span class="Lsub">Save</span> <span class="Rsub">Ctrl+S</span></li>
                                    <li id="save_as"><span>Save As</span> </li>
                                    <li id="exit_btn"><span>Exit</span></li>
                                </ul>
                            </li>
                            <li class="menu_li"><span id="menu2">Edit</span>
                                <ul class="SUBmenu_ul" id="sub2">
                                    <li id="cut"><span class="Lsub">Cut</span> <span class="Rsub">Ctrl+X</span></li>
                                    <li id="copy"><span class="Lsub">Copy</span> <span class="Rsub">Ctrl+C</span></li>
                                    <li id="paste"><span class="Lsub">Paste</span> <span class="Rsub">Ctrl+V</span></li>
                                    <li id="delete"><span class="Lsub">Delete</span> <span class="Rsub">Del</span></li>
                                    <li id="find"><span class="Lsub">Find Next</span> <span class="Rsub">F3</span></li>
                                    <li id="replace"><span class="Lsub">Replace</span> <span class="Rsub">Ctrl+H</span></li>
                                    <li id="goto"><span class="Lsub">Go To</span> <span class="Rsub">Ctrl+G</span></li>
                                    <li id="select"><span class="Lsub">Select All</span> <span class="Rsub">Ctrl+A</span></li>
                                    <li id="timedate"><span class="Lsub">Time/Date</span> <span class="Rsub">F5</span></li>
                                </ul>
                            </li>
                            <li class="menu_li"><span id="menu3">Format</span>
                                <ul class="SUBmenu_ul" id="sub3">
                                    <li><span>Word Wrap</span> </li>
                                    <li id="font"><span>Font</span></li>   
                                </ul>
                            </li>
                            <li class="menu_li"><span id="menu4">Build</span>
                                <ul class="SUBmenu_ul" id="sub4">
                                    <li id="compile"><span>Compile</span> </li>
                                    <li id="run"><span>Run</span></li>  
                                </ul>
                            </li>
                            <li class="menu_li"><span id="menu5">Help</span>
                                <ul class="SUBmenu_ul" id="sub5">
                                    <li id="about"><span>About Notepad</span></li>   
                                </ul>
                            </li>
                        </ul>
                    </div><!--menu-->
                    <div class="menu">
                        <ul class="menu_icon">
                            <li id="tool_save"><span><img src="images/save.png" /></span></li>
                            <li id="tool_new"><span ><img src="images/new.png" /></span></li>
                            <li id="tool_open"><span ><img src="images/open.png" /></span></li>
                            <li class="border_li">
                                <span class="wrap_icons" id="tool_cut"><img src="images/cut.png" /></span>
                                <span class="wrap_icons" id="tool_copy"><img src="images/copy.png" /></span>
                                <span class="wrap_icons" id="tool_paste"><img src="images/paste.png" /></span></li>
                            <li class="border_li">
                                <span class="wrap_icons" id="tool_bold"><img src="images/bold.png" /></span>
                                <span class="wrap_icons" id="tool_italic"><img src="images/italic.png" /></span>
                                <span class="wrap_icons" id="tool_underline"><img src="images/underline.png" /></span>
                            </li>
                            <li><p>Select a theme: <select id="select">
                            <option selected>default</option>
                            <option>3024-day</option>
                            <option>3024-night</option>
                            <option>abcdef</option>
                            <option>ambiance</option>
                            <option>base16-dark</option>
                            <option>base16-light</option>
                            <option>bespin</option>
                            <option>blackboard</option>
                            <option>cobalt</option>
                            <option>colorforth</option>
                            <option>dracula</option>
                            <option>eclipse</option>
                            <option>elegant</option>
                            <option>erlang-dark</option>
                            <option>hopscotch</option>
                            <option>icecoder</option>
                            <option>isotope</option>
                            <option>lesser-dark</option>
                            <option>liquibyte</option>
                            <option>material</option>
                            <option>mbo</option>
                            <option>mdn-like</option>
                            <option>midnight</option>
                            <option>monokai</option>
                            <option>neat</option>
                            <option>neo</option>
                            <option>night</option>
                            <option>paraiso-dark</option>
                            <option>paraiso-light</option>
                            <option>pastel-on-dark</option>
                            <option>railscasts</option>
                            <option>rubyblue</option>
                            <option>seti</option>
                            <option>solarized dark</option>
                            <option>solarized light</option>
                            <option>the-matrix</option>
                            <option>tomorrow-night-bright</option>
                            <option>tomorrow-night-eighties</option>
                            <option>ttcn</option>
                            <option>twilight</option>
                            <option>vibrant-ink</option>
                            <option>xq-dark</option>
                            <option>xq-light</option>
                            <option>yeti</option>
                            <option>zenburn</option>
</select>
</p></li>
                        </ul>
                    </div><!--menu-->
                    <div class="menu">
                        <textarea class="textarea" rows="" cols="" id="write_area"></textarea>
                       <script>
                         editor = CodeMirror.fromTextArea(document.getElementById("write_area"), {
                                    lineNumbers: true,
                                    matchBrackets: true,
                                    styleActiveLine: true,
                                    autoCloseBrackets: true,
                                    theme: 'vibrant-ink',
                                    mode: "text/x-csrc",
                                    mode: "text/x-c++src",
                                    mode: "text/x-java"
                                    
                                    
                        });
                        
                        var mac = CodeMirror.keyMap.default == CodeMirror.keyMap.macDefault;
                        CodeMirror.keyMap.default[(mac ? "Cmd" : "Ctrl") + "-Space"] = "autocomplete";
                        $("#select").change(function(){
                            //var input = document.getElementById("select");
                             var theme = this.options[input.selectedIndex].textContent;
                             editor.setOption("theme", theme);
                        });
	 
                          </script>
                        <!--error_message-->
                        <div class="error_msg" id="er_window" style="margin-top: 5px;border: 1px solid #5e7e9d;">
                            <div class="head">
                                <span class="logo_icon"><img src="images/logo.png" /></span><span class="notepad" id="title_IDE">Output</span>
                                <span class="cross" id="er_cross"><img src="images/cross.png" /></span>
                            </div>
                            <textarea class="out_textarea" rows="" cols="" id="output_area"></textarea>
                        </div>
                        <!--end error_msg-->
                    </div><!--menu-->
                     

                    <!--font dialog box--->
                    <div class="dialog" id="font_dialog">
                        <div class="dialog_upper">
                            <div class="dialog_box">
                                <div class="head" style="border-bottom:1px solid #5e7e9d;"><span class="logo_icon"><img src="images/font.png" /></span><span class="notepad">Font</span>
                                    <span class="cross" id="cross1"><img src="images/cross.png" /></span>
                                </div>
                                <ul class="DialogBox_ul">
                                    <input type="text" id="text_name" class="stle"/>    
                                    <input type="text" id="text_style" class="stle"/>
                                    <input type="text" id="text_size" class="stle"/>
                                    <li class="DialogBox_li"><div class="Dialog_Head"><span>Font</span></div>
                                        <ul class="font_ul" id="font_name">
                                            <jsp:include page="font.jsp"/>
                                        </ul>
                                    </li>
                                    <li class="DialogBox_li"><div class="Dialog_Head"><span>Font Style</span></div>
                                        <ul class="font_ul" id="font_style">
                                            <li><span id="font_lbl">Regular</span></li>
                                            <li><span id="font_lbl">Bold</span></li>
                                            <li><span id="font_lbl">Italic</span></li>
                                            <li><span id="font_lbl">Bold Italic</span></li>
                                        </ul>
                                    </li>
                                    <li class="DialogBox_li"><div class="Dialog_Head"><span>Font Size</span></div>
                                        <ul class="font_ul" id="font_size">
                                            <%
                                                for (int i = 8; i <= 30; i++) {
                                                    out.println("<li><span id='font_lbl'>" + i + "</li></span>");
                                                }
                                            %>
                                        </ul>
                                    </li>
                                    <li class="FontSample_li"><span class="font_sample">Sample</span>
                                        <span class="sample_text" id="sample_text">Abcdefgh</span>
                                    </li>
                                    <li class="dialog_btn"><input type="submit" class="Fbutton" value="Cancel" id="cancel1"/><input type="submit" class="Fbutton" value="Ok" id="font_ok" /></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--end font dialog box--->

                    <!--open dialog-->
                    <div class="dialog" id="open_dialog">
                        <div class="dialog_upper">
                            <div class="dialog_box" id="open_dialog_dynamic">
                                <!--open dialog dynamic content-->    
                            </div>
                        </div>
                    </div>
                    <!--end open dialog-->

                    <!--save dialog-->
                    <div class="dialog" id="save_dialog" >
                        <div class="dialog_upper">
                            <div class="dialog_box" id="save_dialog_dynamic" lang="abc">    
                                <!--Save as dialog dynamic content-->    
                            </div>
                        </div>
                    </div>
                    <!--end save dialog-->
                    <!--find dialog-->
                    <div class="dialog" id="find_dialog" >
                        <div class="dialog_upper">
                            <div class="dialog_box" style="margin-top:300px; background:#efefef;">
                                <div class="head" style=" border-bottom:1px solid #5e7e9d;"><span class="logo_icon"><img src="images/find.png" /></span><span class="notepad">Find</span>
                                    <span class="cross" id="cross6"><img src="images/cross.png" /></span>
                                </div>
                                <div class="dialog_btn" style="margin:10px 0;">
                                    <span style="float:left; margin:3px 22px;">Find What</span>
                                    <input type="text" class="open_list" style="float:left; width:80%;" id="find_text"/>
                                </div>
                                <div class="dialog_btn">
                                    <span style="float:left; margin:37px 0 0 20px;"><input type="checkbox" id="match_case"/>Match Case</span>
                                    <span style="float:right; margin-top:12px;"><input type="submit" class="Fbutton" value="Cancel" id="cancel6"/><input type="submit" class="Fbutton" value="Ok" id="find_next"/></span>
                                    <fieldset class="title_direction">
                                        <legend>Direction</legend>
                                        <input type="radio" name="dir" />Up<input type="radio" name="dir" checked="checked"/>Down
                                    </fieldset>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end find dialog-->
                    <!--replace dialog-->
                    <div class="dialog" id="replace_dialog">
                        <div class="dialog_upper">
                            <div class="dialog_box" style="margin-top:300px; background:#efefef;">
                                <div class="head" style=" border-bottom:1px solid #5e7e9d;"><span class="logo_icon"><img src="images/find.png" /></span><span class="notepad">Replace</span>
                                    <span class="cross" id="cross8"><img src="images/cross.png" /></span>
                                </div>
                                <div class="dialog_btn" style="margin:10px 0;">
                                    <span style="float:left; margin:3px 10px; width:100px;">Find What</span>
                                    <input type="text" class="open_list" style="float:left; width:79%;"  id="replace_find_text"/>
                                </div>
                                <div class="dialog_btn" style="margin:10px 0;">
                                    <span style="float:left; margin:3px 10px; width:100px;">Replace With</span>
                                    <input type="text" class="open_list" style="float:left; width:79%;"  id="replace_replace_text"/>
                                </div>
                                <div class="dialog_btn">
                                    <span style="float:left; margin:37px 0 0 20px;"><input type="checkbox" id="replace_match_case" />Match Case</span>
                                    <span style="float:right; margin-top:12px;"><input type="submit" class="Fbutton" value="Cancel" id="cancel8"/><input type="submit" class="Fbutton" value="Replace All" id="replace_all" /><input type="submit" class="Fbutton" value="Replace" id="replace_text" /><input type="submit" class="Fbutton" value="Find Next" id="replace_find_next"/></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end replace dialog-->
                    <!--goto dialog-->
                    <div class="dialog" id="goto_dialog" style="width:300px; margin-top:200px; " >
                        <div class="dialog_upper" style="width:300px; " >
                            <div class="dialog_box" style="margin-top:150px; background:#efefef;">
                                <div class="head" style=" border-bottom:1px solid #5e7e9d;"><span class="logo_icon"><img src="images/goto.png" /></span><span class="notepad">Go To Line</span>
                                    <span class="cross" id="cross7"><img src="images/cross.png" /></span>
                                </div>
                                <div class="dialog_btn" style="margin:10px 0;">
                                    <span style="float:left; margin:3px 22px;">Line Number</span>
                                    <input type="text" class="open_list" style="float:left; width:85%; margin:0 20px;" />

                                </div>
                                <div class="dialog_btn"> 
                                    <span style="float:right; margin-top:12px;"><input type="submit" class="Fbutton" value="Cancel" id="cancel7"/><input type="submit" class="Fbutton" value="Ok" /></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end goto dialog-->
                    <!--About Notepad dialog-->
                    <div class="dialog" id="about_dialog">
                        <div class="dialog_upper">
                            <div class="dialog_box">
                                <div class="head" style=" border-bottom:1px solid #5e7e9d;"><span class="logo_icon"><img src="images/about_icon.png" /></span><span class="notepad">About Notepad</span>
                                    <span class="cross" id="cross5"><img src="images/cross.png" /></span>
                                </div>
                                <div class="abt_notepad"><span><img src="images/about.png"/></span><span class="abt_heading">ABOUT OUR SOFTWARE</span></div>
                                <div class="abt_text">
                                    <span class="bold_text">Online Compiler IDE</span>
                                    <span class="bold_text">Version 6.1 (Build 7600)</span>
                                    <span class="abt_content">Online Compiler IDE is a generic text editor included with Microsoft Windows that enables someone to open and read plaintext files. If the file contains special formatting or is not a plaintext file, it will not be able to be read in Microsoft Notepad. In the image to the right, is a small example of what the Microsoft Notepad may look like while running.<br/><br/>
                                        
                                    </span>
                                </div>
                                <div  class="dialog_btn"><input type="submit" class="Fbutton" value="Ok" id="cancel5"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end About Notepad dialog-->
                    <!--Replace dialog-->
                    <div class="dialog" id="question_file_dialog" style="width:300px; margin-top:0px; " >
                        <div class="dialog_upper" style="width:300px;" >
                            <div class="dialog_box" style="margin-top:100px; background:#efefef;">
                                <div class="head" style=" border-bottom:1px solid #5e7e9d;"><span class="logo_icon"><img src="images/logo.png" /></span><span class="notepad">Online Compiler IDE</span>
                                    <span class="cross" id="cross11"><img src="images/cross.png" /></span>
                                </div>
                                <div class="dialog_btn" style="margin:10px 0;">
                                    <span style="float:left; margin:3px 22px;">Do you want to save changes to untitled?</span>
                                </div>
                                <div class="dialog_btn"> 
                                    <span style="float:right; margin-top:12px;"><input type="submit" class="Fbutton" value="Cancel" id="cancel11" style="height:30px; width:80px;" /><input type="submit" class="Fbutton" value="No" id="file_dialog_no" style="height:30px; width:80px;" /><input type="submit" class="Fbutton" value="Yes" id="file_dialog_yes" style="height:30px; width:80px;" /></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end replace file dialog-->
                </div><!--wrapper-->
            </div><!--out wrapper-->
        </div>
                                        
    </body>
    
    
   
</html>
