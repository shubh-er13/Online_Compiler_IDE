<%@page import="java.io.File"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder"%>
<%@page import="org.jdom2.Document"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Knowledge Base</title>
<link href="style.css" rel="stylesheet" type="text/css" />
    <script  type="text/javascript" src="scripts/jquery-1.9.0.min.js"></script>
<script type="text/javascript">
    var i = 0;
    $(document).ready(function () {
        $("#answer_wrap li a").click(function () {
            if ($(this).text() == 'Give Your Answer') {
                
                $("#" + $(this).attr("title")).show();
                $(this).text("Close");
            }
            else {
                $("#" + $(this).attr("title")).hide();
                $(this).text("Give Your Answer");

            }
            return false;
        });
     });
</script>

</head>
<body>
<div class="main_wrapper">
<div class="main_head">
    <div class="logo"><img src="images/img.png" width="120" height="100"/></div>
    <ul class="head_menu">
        <li><a href="index.jsp">Logout</a></li>
        <li><a href="#">Help</a></li>
        <li><a href="knowledge_base.jsp">Knowledge Base</a></li>
        <%
            String name=session.getAttribute("name").toString();
            String gender=session.getAttribute("gen").toString();
        %>
        <li><a><%=name%></a></li>
        <%
            if(gender.equals("male"))
            {
        %>
        <li><div class="user_img"><img src="images/male_user.png" /></div></li>        
        <%
            }
            else
            {
        %>
        <li><div class="user_img"><img src="images/user.png" /></div></li>        
        <%
             }
        %>
    </ul>
</div>
<div class="out_wrapper" style="">
<div class="wrapper" style="border-top:none; margin:0; height:auto;">
<div class="heading">Knowledge Base</div>
<ul class="qus_ans" id="answer_wrap">
<%
        String path = application.getRealPath("/") + "knowledgebase.xml";
        Document doc = null;
        SAXBuilder builder = new SAXBuilder();
        File xmlFile = new File(
                path);
        try {
            doc = (Document) builder.build(xmlFile);
        } catch (Exception e) {
            return;
        }
        Element rootNode = doc.getRootElement();
        List l = rootNode.getChildren();
        Iterator itr = l.iterator();
        while(itr.hasNext())
        {
            Element e=(Element)itr.next();
            List qans=e.getChildren();
            Iterator itr1=qans.iterator();
            Element ques=(Element)itr1.next();
%>    
<li>
<div class="question"><%=ques.getText()%></div>
<%
            while(itr1.hasNext())
            {
                Element ans=(Element)itr1.next();
%>
<div class="answer"><b><%=ans.getAttribute("user").getValue()%> said: </b>&nbsp;<%=ans.getText()%></div>
<%
            }
%>
<div class="give_yourans"><a href="#" class="your_ans" title="a<%=e.getAttribute("qno").getValue().toString()%>">Give Your Answer</a></div>
<div class="reply" id="a<%=e.getAttribute("qno").getValue().toString()%>">
<form name="frm" action="updateans">    
<input type="hidden" name="qno" value="<%=e.getAttribute("qno").getValue().toString()%>"/>    
<textarea rows="" cols="" name="yourans" class="reply_textarea"></textarea>
<input type="submit" value="post" class="Rbutton" />
</form>
</div>
</li>
<%
        }
%>

</ul>
<div class="post_block">
    <form name="frm" action="insertq">
    <input type="text" name="ques" class="reply_text" placeholder="Enter your Quetion"/>
    <textarea rows="" cols="" name="ans" class="reply_textarea" style="height:200px;" placeholder="Enter your Answer"></textarea>
    <input type="submit" value="post" class="Fbutton" style="margin-right:0;" />
    </form>
</div>
</div>
</div>
</div>
</body>
</html>