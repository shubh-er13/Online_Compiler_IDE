<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Signup Page</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script>
    var gender="";
    function validate(frm)
    {
        var error=document.getElementById("err");
        error.innerHTML="";
        if(frm.fname.value=="")
        {
            error.innerHTML="Please input first name";
            frm.fname.focus();
            return false;
        }
        else if(validateAlpha(frm.fname.value))
        {
            error.innerHTML="Please input valid first name";
            frm.fname.focus();
            return false;
        }
        else if(frm.lname.value=="")
        {
            error.innerHTML="Please input last name";
            frm.lname.focus();
            return false;
        }
        else if(validateAlpha(frm.lname.value))
        {
            error.innerHTML="Please input valid last name";
            frm.lname.focus();
            return false;
        }
        else if(gender=="")
        {
            error.innerHTML="Please select gender";
            return false;
        }
        else if(frm.mob.value=="")
        {
            error.innerHTML="Please input Mobile";
            frm.mob.focus();
            return false;
        }
        else if(validateNumeric(frm.mob.value)||frm.mob.value.length!=10)
        {
            error.innerHTML="Please input valid Mobile";
            frm.mob.focus();
            return false;
        }
        else if(frm.userid.value=="")
        {
            error.innerHTML="Please input userid";
            frm.userid.focus();
            return false;
        }
        else if(frm.passwd.value=="")
        {
            error.innerHTML="Please input password";
            frm.passwd.focus();
            return false;
        }
        else if(frm.passwd.value.length<4||frm.passwd.value.length>20)
        {
            error.innerHTML="Please input password greater than 4 and less than 20";
            frm.passwd.focus();
            return false;
        }
        else if(frm.cpasswd.value=="")
        {
            error.innerHTML="Please input confirm password";
            frm.cpasswd.focus();
            return false;
        }
        else if(frm.cpasswd.value.length<4||frm.cpasswd.value.length>20)
        {
            error.innerHTML="Please input password greater than 4 and less than 20";
            frm.cpasswd.focus();
            return false;
        }
        else if(frm.cpasswd.value.length!=frm.passwd.value.length)
        {
            error.innerHTML="Password do not match";
            frm.cpasswd.focus();
            return false;
        }
        else if(frm.country.value=="")
        {
            error.innerHTML="Please input country";
            frm.country.focus();
            return false;
        }
        else if(frm.city.value=="")
        {
            error.innerHTML="Please input city";
            frm.city.focus();
            return false;
        }
        else if(frm.month.selectedIndex==0)
        {
            error.innerHTML="Please select month";
            frm.month.focus();
            return false;
        }
        else if(frm.day.selectedIndex==0)
        {
            error.innerHTML="Please select date";
            frm.day.focus();
            return false;
        }
        else if(frm.year.selectedIndex==0)
        {
            error.innerHTML="Please select year";
            frm.year.focus();
            return false;
        }
        else if(frm.email.value=="")
        {
            error.innerHTML="Please select Email";
            frm.email.focus();
            return false;
        }
        return true;
    }
    
    function validateAlpha(text)
    {
        var req="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var i;
        var b=false;
        for(i=0;i<text.length;i++)
        {
             if(req.indexOf(text.charAt(i))==-1)
             {
                    b=true;
                    break;
             }
        }
        return b;        
    }
    function validateNumeric(text)
    {
        var req="0123456789";
        var i;
        var b=false;
        for(i=0;i<text.length;i++)
        {
             if(req.indexOf(text.charAt(i))==-1)
             {
                    b=true;
                    break;
             }
        }
        return b;        
    }
    function selectGender(rad)
    {
        gender=rad.value;
    }
</script>    
</head>
<body>
<div class="main_wrapper">
<div class="main_head"></div>
<div class="background_bg" id="bg"></div>
<div class="out_wrapper" style="width:800px; ">
<div class="wrapper" style="height:auto; background:#ACC3D9	;">
<div class="head">
<span class="logo_icon"><img src="images/logo.png" /></span><span class="notepad">Online Compiler IDE Login</span>
</div><!--head-->
<div class="menu">
<div class="menu_ul"><span id="err"></span></div>
<ul class="login_ul" style="width:98.7%;">
    <form name="frm" action="register" method="post" onsubmit="return validate(this)">    
<li><span class="reg_heading">SignUP Here</span></li>
<li class="small"><span class="reg_span">First Name</span><input type="text" class="reg_textbox" name="fname"/></li>
<li class="small"><span class="reg_span">Last Name</span><input type="text" class="reg_textbox" name="lname"/></li>
<li class="small"><span class="reg_span">Gender</span><input type="radio" name="gender" value="male"  style="margin: 20px 0;" onclick="selectGender(this)"/> Male<input type="radio" name="gender" value="female" style="margin: 20px 0 0 20px;"  onclick="selectGender(this)"/> Female</li>
<li class="small"><span class="reg_span">Mobile No.</span><input type="text" name="mob" class="reg_textbox" /></li>
<li><span class="reg_span">Choose User ID</span><input type="text" name="userid" class="reg_textbox" style="width:75%;" /></li>
<li class="small"><span class="reg_span">Password</span><input type="password" name="passwd" class="reg_textbox" /></li>
<li class="small"><span class="reg_span">Confirm Password</span><input type="password" name="cpasswd" class="reg_textbox" /></li>
<li class="small"><span class="reg_span">Your Country</span><input type="text" name="country" class="reg_textbox" /></li>
<li class="small"><span class="reg_span">Your City</span><input type="text" name="city" class="reg_textbox" /></li>
<li><span class="reg_span">Date of Birth</span>
<select class="reg_select" name="month">
    <option>Month</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
</select>
<select class="reg_select" name="day">
    <option>Day</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="10">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option>
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">20</option>
    <option value="31">31</option>
</select>
<select class="reg_select" name="year">
    <option>Year</option>
    <%
        for(int i=1986;i<=2010;i++)
        {
    %>
    <option value="<%=i%>"><%=i%></option>
    <%
        }
    %>
</select>
</li>
<li><span class="reg_span">Current Email ID</span><input type="text" class="reg_textbox" style="width:75%;" name="email"/></li>
<li><input type="submit" class="Lbutton" style="margin-right:20px;"/></li>
</form>
</ul>
</div>
</div><!--wrapper-->
</div><!--out wrapper-->
</div>
</body>
</html>


