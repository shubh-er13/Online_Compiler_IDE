import javax.servlet.http.*;
import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
public class RegistrationServlet extends HttpServlet
{
    String s10;
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        try{
        String s1=req.getParameter("fname");
        String s2=req.getParameter("lname");
        String s3=req.getParameter("gender");
        String s4=req.getParameter("mob");
        String s5=req.getParameter("userid");
        String s6=req.getParameter("cpasswd");
        String s7=req.getParameter("country");
        String s8=req.getParameter("city");
        String s9=req.getParameter("day")+"/"+req.getParameter("month")+"/"+req.getParameter("year");
        s10=req.getParameter("email");
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","123");
        Statement st=con.createStatement();
        String tempid=getTempid();
        st.executeUpdate("insert into users values('"+s1+"','"+s2+"','"+s3+"','"+s4+"','"+s5+"','"+s6+"','"+s7+"','"+s8+"','"+s9+"','"+s10+"','N','"+tempid+"')");
        st.close();
        con.close();
        send(s10,tempid);
        }catch(Exception e)
        {
            System.out.println(e);
        }
        req.setAttribute("email",s10);
        RequestDispatcher rd=getServletContext().getRequestDispatcher("/verify.jsp");
        rd.forward(req, resp);
    }
   public void send(String email,String tempid) throws UnknownHostException
    {
        String from = "compiler.dac@gmail.com";
        String host = "smtp.gmail.com";
        String userid = "compiler.dac"; 
        String password = "compiler_dac"; 
        String subject="Welcome";
        String CONTENT_CHARSET = "text/html; charset=utf-8";
        try
        {
            Properties props = System.getProperties(); 
            props.put("mail.smtp.starttls.enable", "true"); 
            props.put("mail.smtp.host", host); 
            props.setProperty("mail.transport.protocol", "smtps");
            props.put("mail.smtp.user", userid); 
            props.put("mail.smtp.password", password); 
            props.put("mail.smtp.port", "465"); 
            props.put("mail.smtps.auth", "true"); 
            Session session = Session.getDefaultInstance(props, null); 
            MimeMessage message = new MimeMessage(session); 
            InternetAddress fromAddress = null;
            InternetAddress toAddress = null;
            String servAddr = InetAddress.getLocalHost().getHostAddress();
            try {
                fromAddress = new InternetAddress(from);
                toAddress = new InternetAddress(email);
            } catch (AddressException e) {
                e.printStackTrace();
            }
            message.setFrom(fromAddress);
            message.setRecipient(Message.RecipientType.TO, toAddress);
            message.setSubject(subject);
            String body="Action Required:<br><br>Verify your e-mail address by clicking this link:<br>http://"+servAddr+":8084/verified.jsp?tempid="+tempid+"<br><br>Warm Regards,<br>Admin";
            message.setContent(body,CONTENT_CHARSET); 
            Transport transport = session.getTransport("smtps"); 
            transport.connect(host, userid, password); 
            transport.sendMessage(message, message.getAllRecipients()); 
            transport.close(); 
            } catch (MessagingException e) {
            e.printStackTrace();
            }
    }
    
    public String getTempid()
    {
        String req="0123456789abcedffghijklmnopqrstuvwxyzABCEDFGHIJKLMNOPQRSTUVWXYZ";
        String tempid="";
        int len=req.length();
        for(int i=1;i<=10;i++)
        {
            int index=(int)(len*Math.random());
            tempid+=req.charAt(index);
        }
        return tempid;
    }
}