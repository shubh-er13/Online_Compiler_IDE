import javax.servlet.http.*;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
public class LoginServlet extends HttpServlet
{
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        String userid=req.getParameter("userid");
        String passwd=req.getParameter("passwd");
        HttpSession ses=req.getSession();
        if(userid!=null&&passwd!=null)
        {
            try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","123");
            Statement st=con.createStatement();
            ResultSet rs=st.executeQuery("select * from users where userid='"+userid+"' and passwd='"+passwd+"'");
            if(rs.next())
            {
                if(rs.getString("verify").equals("N"))
                {
                    resp.sendRedirect("index.jsp?err="+getTempId());
                }
                else
                {
                    ses.setAttribute("userid", rs.getString(5));
                    ses.setAttribute("name", rs.getString(1)+" "+rs.getString(2));
                    ses.setAttribute("gen", rs.getString(3));
                    ses.setAttribute("filename", "Untitled");
                    File f1=new File(getServletContext().getRealPath("/")+rs.getString(5)+File.separator+"Java");
                    File f2=new File(getServletContext().getRealPath("/")+rs.getString(5)+File.separator+"C");
                    if(!f1.exists())
                        f1.mkdirs();
                    if(!f2.exists())        
                        f2.mkdirs();
                   resp.sendRedirect("welcome.jsp");
                }
            }
            else
            {
                resp.sendRedirect("index.jsp?msgerr="+getTempId());
            }
            rs.close();
            st.close();
            con.close();
            }catch(Exception e)
            {
                System.out.println(e);
            }
        }
        else 
        {
            resp.sendRedirect("login.jsp");
        }
    }
    public String getTempId()
    {
        String req="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        String tempid="";
        int len=req.length();
        for(int i=1;i<=10;i++)
        {
            int index=(int)(len*Math.random());
            tempid+=req.charAt(index);
        }
        return tempid;
    }
}