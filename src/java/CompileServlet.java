
import java.io.*;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

public class CompileServlet extends HttpServlet
{
    String path,parent,output="";
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        String filename=req.getParameter("filename").trim();
        System.out.println(filename);
        HttpSession ses=req.getSession();
        String userid=ses.getAttribute("userid").toString();
        if(filename.endsWith(".java"))
        {
            path=getServletContext().getRealPath("/")+userid+File.separator+"Java"+File.separator+filename;
            parent=getServletContext().getRealPath("/")+userid+File.separator+"Java"+File.separator;
           // String opts[]={"-g","-source","1.7","-target","1.7","-d",parent,path};
           // Process p = Runtime.getRuntime().exec("cmd /c javac "+path);
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            PrintWriter pw=new PrintWriter(parent+"err.txt");
            FileInputStream fin=new FileInputStream(parent+"err.txt");
            FileOutputStream out = new FileOutputStream(parent+"err.txt");
            compiler.run(null, out, null, path);
            byte b[]=new byte[fin.available()];
            fin.read(b);
            fin.close();
            output=new String(b);
        }
        else if(filename.endsWith(".c"))
        {
            path=getServletContext().getRealPath("/")+userid+File.separator+"C"+File.separator+filename;
            parent=getServletContext().getRealPath("/")+userid+File.separator+"C";
            Process p1=Runtime.getRuntime().exec("cmd /c gcc "+filename,null,new File(parent));
            System.out.println(parent);
            DataInputStream din=new DataInputStream(p1.getErrorStream());
            String o=null;
            while((o=din.readLine())!=null)
            {
                output+=o+"\n";
            }
            din.close();
        }
        
        PrintWriter out=resp.getWriter();
        out.println(output);
        out.close();
        output="";
    }
    
}