import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class SaveServlet extends HttpServlet
{
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        System.out.println("Hello World");
        PrintWriter pw=resp.getWriter();
        HttpSession ses=req.getSession();
        String filepath=req.getParameter("filename");
        String name=ses.getAttribute("name").toString();
        name=name.substring(0, name.indexOf(" "));
        String str="ftp://server/"+name+"/";
        filepath=filepath.substring(filepath.indexOf(str)+str.length());
        String fileparent=getServletContext().getRealPath("/")+ses.getAttribute("userid")+File.separator+filepath;
        String filename=fileparent.substring(fileparent.lastIndexOf("/")+1);
        ses.setAttribute("filepath", fileparent);
        ses.setAttribute("filename", filename);
        String content=req.getParameter("content");
        if(new File(fileparent).exists())
        {
            pw.println("1");
        }
        else
        {
            FileOutputStream fout=new FileOutputStream(getServletContext().getRealPath("/")+ses.getAttribute("userid")+File.separator+filepath);
            fout.write(content.getBytes());
            fout.close();
            pw.println(filename);
        }
    }
}
