
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class DownloadServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        HttpSession ses=req.getSession();
        String filepath=req.getParameter("filename");
        String name=ses.getAttribute("name").toString();
        name=name.substring(0, name.indexOf(" "));
        String str="ftp://server/"+name+"/";
        filepath=filepath.substring(filepath.indexOf(str)+str.length());
        String fileparent=getServletContext().getRealPath("/")+ses.getAttribute("userid")+File.separator+filepath;
        String filename=fileparent.substring(fileparent.lastIndexOf("/")+1);
        FileInputStream fin=new FileInputStream(fileparent);
        byte b[]=new byte[fin.available()];
        fin.read(b);
        fin.close();;
        resp.setContentType("multipart/form-data");
        resp.setHeader("Content-disposition", "attachment; filename="+filename);
        ServletOutputStream output=resp.getOutputStream();
        output.write(b);
    }
    
}