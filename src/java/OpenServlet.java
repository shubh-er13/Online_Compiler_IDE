
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class OpenServlet extends HttpServlet
{

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        PrintWriter pw=resp.getWriter();
        HttpSession ses=req.getSession();
        String filepath=req.getParameter("filename");
        String name=ses.getAttribute("name").toString();
        name=name.substring(0, name.indexOf(" "));
        String str="ftp://server/"+name+"/";
        filepath=filepath.substring(filepath.indexOf(str)+str.length());
        String fileparent=getServletContext().getRealPath("/")+ses.getAttribute("userid")+File.separator+filepath;
        String filename=fileparent.substring(fileparent.lastIndexOf("/")+1);
        ses.setAttribute("filepath", fileparent);
        ses.setAttribute("filename", filename);
        FileInputStream fin=new FileInputStream(fileparent);
        byte b[]=new byte[fin.available()];
        fin.read(b);
        fin.close();
        pw.println(new String(b));
        pw.close();
    }
    
}