import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class SaveasServlet extends HttpServlet
{
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        HttpSession ses=req.getSession();
        String filepath=ses.getAttribute("filepath").toString();
        String filename=filepath.substring(filepath.lastIndexOf("/")+1);
        String content=req.getParameter("content");
        FileOutputStream fout=new FileOutputStream(filepath);
        fout.write(content.getBytes());
        fout.close();
        ses.setAttribute("filepath", filepath);
        ses.setAttribute("filename", filename);
        PrintWriter pw=resp.getWriter();
        pw.println(filename);
    }    
}