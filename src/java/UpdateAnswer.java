
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class UpdateAnswer extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int qn = Integer.parseInt(req.getParameter("qno"));
        String ans = req.getParameter("yourans");
        HttpSession ses = req.getSession();
        String userid = ses.getAttribute("userid").toString();
        String path = getServletContext().getRealPath("/") + "knowledgebase.xml";
        Document doc = null;
        SAXBuilder builder = new SAXBuilder();
        File xmlFile = new File(path);
        try {
            doc = (Document) builder.build(xmlFile);
        } catch (Exception e) {
            return;
        }
        Element rootNode = doc.getRootElement();
        List l = rootNode.getChildren();
        Iterator itr = l.iterator();
        Element e=null;
        while(itr.hasNext())
        {
            e=(Element)itr.next();
            int qno=Integer.parseInt(e.getAttribute("qno").getValue().toString());
            if(qno==qn)
                break;
        }
        Element a = new Element("answer");
        a.setAttribute(new Attribute("user", userid));
        a.addContent(ans);
        e.addContent(a);
        //rootNode.addContent(e);
        try {
            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());
            xmlOutput.output(doc, new FileWriter(path));
        } catch (Exception ex) {
            System.out.println(ex);
        }
        resp.sendRedirect("knowledge_base.jsp");
    }
}