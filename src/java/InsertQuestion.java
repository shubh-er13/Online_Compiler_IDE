
import java.io.File;
import java.io.FileWriter;
import java.util.Iterator;
import java.util.List;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class InsertQuestion extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String ques = req.getParameter("ques");
        String ans = req.getParameter("ans");
        HttpSession ses = req.getSession();
        String userid = ses.getAttribute("userid").toString();
        String path = getServletContext().getRealPath("/") + "knowledgebase.xml";
        int qno = 1;
        Document doc = null;
        SAXBuilder builder = new SAXBuilder();
        File xmlFile = new File(path);
        try {
            doc = (Document) builder.build(xmlFile);
        } catch (Exception e) {
            return;
        }
        Element rootNode = doc.getRootElement();
        List l = rootNode.getChildren();
        Iterator itr = l.iterator();
        if (itr.hasNext()) {
            while(itr.hasNext())
            {
                Element e=(Element)(itr.next());
                qno=Integer.parseInt(e.getAttribute("qno").getValue().toString());
            }
            qno=qno+1;
            Element q = new Element("quesans");
            q.setAttribute(new Attribute("qno", qno + ""));
            Element qs = new Element("question");
            qs.addContent(ques);
            Element a = new Element("answer");
            a.setAttribute(new Attribute("user", userid));
            a.addContent(ans);
            q.addContent(qs);
            q.addContent(a);
            rootNode.addContent(q);
        } else {
            Element q = new Element("quesans");
            q.setAttribute(new Attribute("qno", qno + ""));
            Element qs = new Element("question");
            qs.addContent(ques);
            Element a = new Element("answer");
            a.setAttribute(new Attribute("user", userid));
            a.addContent(ans);
            q.addContent(qs);
            q.addContent(a);
            rootNode.addContent(q);
        }
        try {
            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());
            xmlOutput.output(doc, new FileWriter(path));
        } catch (Exception e) {
            System.out.println(e);
        }
        resp.sendRedirect("knowledge_base.jsp");
    }
}