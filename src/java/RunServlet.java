
import java.io.*;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static org.apache.catalina.connector.InputBuffer.DEFAULT_BUFFER_SIZE;

public class RunServlet extends HttpServlet
{
    String path,parent,output="";
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        String filename=req.getParameter("filename").trim();
        HttpSession ses=req.getSession();
        String userid=ses.getAttribute("userid").toString();
        if(filename.endsWith(".java"))
        {
            String classname=filename.substring(0, filename.lastIndexOf(".java"));
            path=getServletContext().getRealPath("/")+userid+File.separator+"Java"+File.separator+classname;
            parent=getServletContext().getRealPath("/")+userid+File.separator+"Java";
            Process p1=Runtime.getRuntime().exec("cmd /c java "+classname,null,new File(parent));
            DataInputStream din=new DataInputStream(p1.getInputStream());
            String o=null;
            while((o=din.readLine())!=null)
            {
                output+=o+"\n";
            }
            din.close();
        }
        else if(filename.endsWith(".c"))
        {
            String exename=filename.substring(0, filename.lastIndexOf(".c"));
            path=getServletContext().getRealPath("/")+userid+File.separator+"C"+File.separator+filename;
            parent=getServletContext().getRealPath("/")+userid+File.separator+"C";
            Process p1=Runtime.getRuntime().exec("cmd /c a.exe",null,new File(parent));
            DataInputStream din=new DataInputStream(p1.getInputStream());
            String o=null;
            while((o=din.readLine())!=null)
            {
                output+=o+"\n";
            }
            din.close();
        }
        
        PrintWriter out=resp.getWriter();
        out.println(output);
        out.close();
        output="";
    }
    
    
}