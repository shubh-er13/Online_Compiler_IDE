package cons;
import java.io.File;
import java.io.FileFilter;
public class MyFilter implements FileFilter
{
    @Override
    public boolean accept(File pathname) 
    {
        String path=pathname.getPath();
        //System.out.println(path);
        if(pathname.isDirectory())
            return true;
        if(path.indexOf("\\Java\\")!=-1)
        {
            if(path.endsWith(".java"))
                return true;
        }
        if(path.indexOf("\\C\\")!=-1)
        {
            if(path.endsWith(".c"))
                return true;
        }
       
        return false;
    }
    
}
