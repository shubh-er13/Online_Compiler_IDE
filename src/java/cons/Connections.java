package cons;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Connections
{
    Connection con;
    Statement st;
    public Statement makeConnection()
    {
        try{
        Class.forName("oracle.jdbc.driver.OracleDriver");
        con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","123");
        st=con.createStatement();
        }catch(Exception e)
        {}
        return st;
    }
    public void closeConnection()
    {
        try{
          st.close();
          con.close();
        }catch(Exception e)
        {}
    }
}
